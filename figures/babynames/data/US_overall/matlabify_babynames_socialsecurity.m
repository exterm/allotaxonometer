more off;

directory = 'yeardata/.';
filestruct = dir(directory);

matfilename = 'yeardata.mat';

clear years babynamedata

j=0;
for i=1:length(filestruct)
    filenamecell = regexp(filestruct(i).name,'yob\d\d\d\d\.txt','match');
    if (~isempty(filenamecell))
        j=j+1;
        filename = sprintf('%s/%s',directory,filenamecell{1});
        fprintf(1,'processing %s ...\n',filename);
        
        clear yobtable;
        yobtable = readtable(filename);
        
        names = yobtable{:,1};
        sexes = yobtable{:,2};
        counts = yobtable{:,3};

        indices = find(strcmp(sexes,'F'));
        
        babynamedata(j,1).names = names(indices);
        babynamedata(j,1).counts = counts(indices);

        indices = find(strcmp(sexes,'M'));

        babynamedata(j,2).names = names(indices);
        babynamedata(j,2).counts = counts(indices);
        
    end
end        

fprintf('saving data ...\n');

Nyears = size(babynamedata,1);
years = col(1880:(1880+Nyears-1));

for i=1:Nyears
    babynamedata(i,1).year = years(i);
    babynamedata(i,2).year = years(i);
end

%% randomize order
for i=1:Nyears
    k=1;
    indices = randperm(length(babynamedata(j,k).names));
    babynamedata(j,k).names = babynamedata(j,k).names(indices);
    babynamedata(j,k).counts = babynamedata(j,k).counts(indices);
    k=2;
    indices = randperm(length(babynamedata(j,k).names));
    babynamedata(j,k).names = babynamedata(j,k).names(indices);
    babynamedata(j,k).counts = babynamedata(j,k).counts(indices);
end

for i=1:Nyears
    babynamedata(i,1).ranks = tiedrank(-babynamedata(i,1).counts);
    babynamedata(i,2).ranks = tiedrank(-babynamedata(i,2).counts);
end


%% totals for each year

totalstable = readtable('births_per_year.txt');

totals = totalstable{:,[3 2 4]};

for i=1:length(babynamedata)
    %% girls then boys
    babynamedata(i,1).totalcounts = totals(i,1);
    babynamedata(i,2).totalcounts = totals(i,2);

    babynamedata(i,1).probs = ...
        babynamedata(i,1).counts ./ ...
        babynamedata(i,1).totalcounts;
    babynamedata(i,2).probs = ...
        babynamedata(i,2).counts ./ ...
        babynamedata(i,2).totalcounts;

    coverages(i,1) = sum(babynamedata(i,1).probs);
    coverages(i,2) = sum(babynamedata(i,2).probs);
end

save babynamedata babynamedata years totals coverages;

%%%%%%%%%%%%%%%%
%% time series
%% add container.Map for names and time series

for i=1:size(babynamedata,1)
    maxranks(i,1) = max(babynamedata(i,1).ranks);
    maxranks(i,2) = max(babynamedata(i,2).ranks);
end

save babynamedata babynamedata years totals coverages ...
    maxranks;

girlname_timeseries_Map = containers.Map();
boyname_timeseries_Map = containers.Map();


%%%%%%%%%
%% girls
%%%%%%%%%

k=1; 
for i=1:Nyears
    fprintf(1,'Time series for girls: %d ...\n',years(i));
    for j=1:length(babynamedata(i,k).names);
        name = babynamedata(i,k).names{j};
        if (~isKey(girlname_timeseries_Map,name))
            girlname_timeseries_Map(name) = zeros(0,4);
        end
        girlname_timeseries_Map(name) = [girlname_timeseries_Map(name); ...
                            babynamedata(i,k).year, ...
                            babynamedata(i,k).counts(j), ... 
                            babynamedata(i,k).ranks(j), ... 
                            babynamedata(i,k).probs(j)];
     end
end

%% sum up 1/r^alpha and probabilities for each name for overall ranking

alpha = 0.25;
names = keys(girlname_timeseries_Map);
for i=1:length(names)
    namedata = girlname_timeseries_Map(names{i});
    girlname_summary{i,1} = names{i};
    girlname_summary{i,2} = size(namedata,1);
    girlname_summary{i,3} = sum(1./namedata(:,3).^alpha); %% inverse power
                                                       %% of ranks 
    girlname_summary{i,4} = sum(namedata(:,4)); %% sum of probs
end

[~,indices] = sort(cell2mat(girlname_summary(:,3)),'descend');
girlname_summary = girlname_summary(indices,:);

%%%%%%%%%
%% boys
%%%%%%%%%

k=2; 
for i=1:Nyears
    fprintf(1,'Time series for boys: %d ...\n',years(i));
    for j=1:length(babynamedata(i,k).names);
        name = babynamedata(i,k).names{j};
        if (~isKey(boyname_timeseries_Map,name))
            boyname_timeseries_Map(name) = zeros(0,4);
        end
        boyname_timeseries_Map(name) = [boyname_timeseries_Map(name); ...
                             babynamedata(i,k).year, ...
                            babynamedata(i,k).counts(j), ... 
                            babynamedata(i,k).ranks(j), ... 
                            babynamedata(i,k).probs(j)];
     end
end

%% sum up 1/r^alpha and probabilities for each name for overall ranking

alpha = 0.25;
names = keys(boyname_timeseries_Map);
for i=1:length(names)
    namedata = boyname_timeseries_Map(names{i});
    boyname_summary{i,1} = names{i};
    boyname_summary{i,2} = size(namedata,1);
    boyname_summary{i,3} = sum(1./namedata(:,3).^alpha); %% inverse power
                                                       %% of ranks 
    boyname_summary{i,4} = sum(namedata(:,4)); %% sum of probs
end

[~,indices] = sort(cell2mat(boyname_summary(:,3)),'descend');
boyname_summary = boyname_summary(indices,:);

save babynamedata_timeseries ...
    girlname_timeseries_Map ...
    boyname_timeseries_Map ...
    *name_summary;


more on;


%% x = girlname_timeseries_Map('Jess')
%% plot(x(:,1),log10(x(:,3)),'-'); set(gca,'ydir','reverse'); xlim([1880 2018]);
%% 