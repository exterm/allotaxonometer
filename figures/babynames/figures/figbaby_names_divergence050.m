more off;

%% makes allotaxonographs that:
%% 1. compare baby names 50 years apart, 10 years, 1880 on
%% 2. varying alpha
%%
%% girls have -1- in file name
%% boys have -2 in file name
%% 
%% does some extra analysis for interesting names
%% 
%% flipbook making turned off (search for combinepdf)


if (~exist('babynamedata'))
    load ../data/US_overall/babynamedata.mat;
end


%% year1 = 1918;
%% year2 = 2018;
%% 
%% year1 = 1973;
%% year2 = 1995;
%% 
%% year1 = 1945;
%% year2 = 1965;
%% 
%% year1 = 1946;
%% year2 = 1966;

yeardelta = 50;
yearskip = 5;
yearvals = (1880:yearskip:(years(end)-yeardelta))';

for i_year = 1:length(yearvals);
    year1 = yearvals(i_year);
    year2 = year1 + yeardelta;

    index1 = year1 - 1880 + 1;
    index2 = year2 - 1880 + 1;

    %% girls

    k=1;
    j=1;
    elements(j).types = babynamedata(index1,k).names;
    elements(j).counts = babynamedata(index1,k).counts;
    elements(j).ranks = babynamedata(index1,k).ranks;
    elements(j).totalunique = length(elements(j).types);

    j=2;
    elements(j).types = babynamedata(index2,k).names;
    elements(j).counts = babynamedata(index2,k).counts;
    elements(j).ranks = babynamedata(index2,k).ranks;
    elements(j).totalunique = length(elements(j).types);
    
    mixedelements = combine_distributions(elements(1),elements(2));

    settings.imageformat.open = 'no';
    settings.maxrank_log10 = 5;
    settings.maxcount_log10 = 4;

    settings.more_string = {'more','popular'};
    settings.less_string = {'less','popular'};
    
    settings.typename = 'name';

    settings.plotkind = 'rank';
    settings.instrument = 'rank divergence';

    settings.topNshuffling = 25;
    settings.topNshift = 40;
    settings.topNdeltasum = 'all';

    settings.xoffset = +0.05;

    settings.max_plot_string_length = 20;
    settings.max_shift_string_length = 25;


    fprintf(1,'constructing figure ...\n');

    settings.system1_name = sprintf('Baby girl names in %d',year1);
    settings.system2_name = sprintf('Baby girl names in %d',year2);

    settings.axislabel_top1 = 'Popularity rank $r$';
    settings.axislabel_top2 = 'Popularity rank $r$';
        
    settings.alpha = Inf;
    %% settings.alpha = 1/3;
    %% settings.alpha = 1/2;
    %% settings.alpha = 5/6;

    tag = sprintf('babynames-rtd-010-alpha-infty-%d-%03d-%d-vs-%d',k,yeardelta,year1,year2);
    figallotaxonometer9000(mixedelements,tag,settings);
    
    %% compute divergence separately
    [divergence_elements,normalization] = ...
        rank_turbulence_divergence(mixedelements,settings.alpha);
    RTDvals(i_year,k) = sum(divergence_elements);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% 
    %% boys

    k=2;
    j=1;
    elements(j).types = babynamedata(index1,k).names;
    elements(j).counts = babynamedata(index1,k).counts;
    elements(j).ranks = babynamedata(index1,k).ranks;
    elements(j).totalunique = length(elements(j).types);

    j=2;
    elements(j).types = babynamedata(index2,k).names;
    elements(j).counts = babynamedata(index2,k).counts;
    elements(j).ranks = babynamedata(index2,k).ranks;
    elements(j).totalunique = length(elements(j).types);

    mixedelements = combine_distributions(elements(1),elements(2));

    settings.imageformat.open = 'no';
    settings.maxrank_log10 = 5;
    settings.maxcount_log10 = 4;
    
    settings.more_string = {'more','popular'};
    settings.less_string = {'less','popular'};

    settings.typename = 'name';
        
    settings.plotkind = 'rank';
    settings.instrument = 'rank divergence';

    settings.topNshuffling = 25;
    settings.topNshift = 40;
    settings.topNdeltasum = 'all';

    settings.xoffset = +0.05;

    settings.max_plot_string_length = 20;
    settings.max_shift_string_length = 25;


    fprintf(1,'constructing figure ...\n');

    settings.system1_name = sprintf('Baby boy names in %d',year1);
    settings.system2_name = sprintf('Baby boy names in %d',year2);
    
    settings.axislabel_top1 = 'Popularity rank $r$';
    settings.axislabel_top2 = 'Popularity rank $r$';

    settings.alpha = Inf;
    %% settings.alpha = 1/3;
    %% settings.alpha = 1/2;
    %% settings.alpha = 5/6;

    tag = sprintf('babynames-rtd-010-alpha-infty-%d-%03d-%d-vs-%d',k,yeardelta,year1,year2);
    figallotaxonometer9000(mixedelements,tag,settings);
    
    %% compute divergence separately
    [divergence_elements,normalization] = ...
        rank_turbulence_divergence(mixedelements,settings.alpha);
    RTDvals(i_year,k) = sum(divergence_elements);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end

%% to make flipbooks:
%% need ghostscript here
%% combinepdfs is from kitchentabletools
%% https://github.com/petersheridandodds/kitchentabletools

%% !combinepdfs figallotaxonometer9000/figallotaxonometer9000-babynames-rtd-010-alpha-infty-1-050-???[05]-vs-????_noname.pdf figallotaxonometer9000/figallotaxonometer9000-babynames-rtd-010-alpha-infty-1-050-????-vs-2018_noname.pdf flipbooks/figallotaxonometer9000-babynames-rtd-010-alpha-infty-1-050-decade-combined.pdf

%% !combinepdfs figallotaxonometer9000/figallotaxonometer9000-babynames-rtd-010-alpha-infty-2-050-???[05]-vs-????_noname.pdf figallotaxonometer9000/figallotaxonometer9000-babynames-rtd-010-alpha-infty-2-050-????-vs-2018_noname.pdf flipbooks/figallotaxonometer9000-babynames-rtd-010-alpha-infty-2-050-decade-combined.pdf


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% vary alpha for 1968 and 2018 comparison, make flipbooks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

yeardelta = 50;
yearvals = (1880:(years(end)-yeardelta))';

i_year = 89;

year1 = yearvals(i_year);
year2 = year1 + yeardelta;

index1 = year1 - 1880 + 1;
index2 = year2 - 1880 + 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% girls
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

k=1;
j=1;
elements(j).types = babynamedata(index1,k).names;
elements(j).counts = babynamedata(index1,k).counts;
elements(j).ranks = babynamedata(index1,k).ranks;
elements(j).totalunique = length(elements(j).types);

j=2;
elements(j).types = babynamedata(index2,k).names;
elements(j).counts = babynamedata(index2,k).counts;
elements(j).ranks = babynamedata(index2,k).ranks;
elements(j).totalunique = length(elements(j).types);

mixedelements = combine_distributions(elements(1),elements(2));

settings.imageformat.open = 'no';
settings.maxrank_log10 = 5;
settings.maxcount_log10 = 4;

settings.more_string = {'more','popular'};
settings.less_string = {'less','popular'};

settings.plotkind = 'rank';
settings.instrument = 'rank divergence';

settings.topNshuffling = 25;
settings.topNshift = 40;
settings.topNdeltasum = 'all';

settings.xoffset = +0.05;

settings.max_plot_string_length = 20;
settings.max_shift_string_length = 25;


fprintf(1,'constructing figure ...\n');

settings.system1_name = sprintf('Baby girl names in %d',year1);
settings.system2_name = sprintf('Baby girl names in %d',year2);

settings.axislabel_top1 = 'Popularity rank $r$';
settings.axislabel_top2 = 'Popularity rank $r$';

%% alphavals = [(0:18)/12, 2, 3, 5, 10, Inf]';

alphavals = [[0 1 2 3 4 6  8]/12, 1, 2, 5, Inf]';

for i=1:length(alphavals)
    %% for i=[1, 2, length(alphavals)]
    %%        for i=1
    settings.alpha = alphavals(i);
    tag = sprintf('babynames-rtd-010-alphasweep-%02d-%d-%03d-%d-vs-%d',i,k,yeardelta,year1,year2);
    figallotaxonometer9000(mixedelements,tag,settings);
end

!combinepdfs figallotaxonometer9000/figallotaxonometer9000-babynames-rtd-010-alphasweep-??-1-*_noname.pdf flipbooks/figallotaxonometer9000-babynames-rtd-010-alphasweep-1-combined.pdf

%% !open flipbooks/figallotaxonometer9000-babynames-rtd-010-alphasweep-1-combined.pdf

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% boys
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

k=2;
j=1;
elements(j).types = babynamedata(index1,k).names;
elements(j).counts = babynamedata(index1,k).counts;
elements(j).ranks = babynamedata(index1,k).ranks;
elements(j).totalunique = length(elements(j).types);

j=2;
elements(j).types = babynamedata(index2,k).names;
elements(j).counts = babynamedata(index2,k).counts;
elements(j).ranks = babynamedata(index2,k).ranks;
elements(j).totalunique = length(elements(j).types);

mixedelements = combine_distributions(elements(1),elements(2));

settings.imageformat.open = 'no';
settings.maxrank_log10 = 5;
settings.maxcount_log10 = 4;

settings.more_string = {'more','popular'};
settings.less_string = {'less','popular'};

settings.plotkind = 'rank';
settings.instrument = 'rank divergence';

settings.topNshuffling = 25;
settings.topNshift = 40;
settings.topNdeltasum = 'all';

settings.xoffset = +0.05;

settings.max_plot_string_length = 20;
settings.max_shift_string_length = 25;


fprintf(1,'constructing figure ...\n');

settings.system1_name = sprintf('Baby boy names in %d',year1);
settings.system2_name = sprintf('Baby boy names in %d',year2);

settings.axislabel_top1 = 'Popularity rank $r$';
settings.axislabel_top2 = 'Popularity rank $r$';

%% alphavals = [(0:18)/12, 2, 3, 5, 10, Inf]';

alphavals = [[0 1 2 3 4 6  8]/12, 1, 2, 5, Inf]';

for i=1:length(alphavals)
    %% for i=[1, 2, length(alphavals)]
    %%        for i=1
    settings.alpha = alphavals(i);
    tag = sprintf('babynames-rtd-010-alphasweep-%02d-%d-%03d-%d-vs-%d',i,k,yeardelta,year1,year2);
    figallotaxonometer9000(mixedelements,tag,settings);
end

!combinepdfs figallotaxonometer9000/figallotaxonometer9000-babynames-rtd-010-alphasweep-??-2-*_noname.pdf flipbooks/figallotaxonometer9000-babynames-rtd-010-alphasweep-2-combined.pdf

%% !open flipbooks/figallotaxonometer9000-babynames-rtd-010-alphasweep-2-combined.pdf

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% extra details for the paper, 1968 and 2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\n');

years_for_show = [1968 2018];
for i=1:length(years_for_show)
    indices(i) = find(years==years_for_show(i));
end
birthsex = {'girl','boy'};

%% girls = column 1, boys = column 2

for k=1:2
    for i=1:length(years_for_show)
        fprintf(1,'Number of unique %s names in %d:\n',birthsex{k},years_for_show(i));
        fprintf(1,'%s\n',addcommas(length(babynamedata(indices(i),k).names)));
        fprintf(1,'Total number of %s names in %d:\n',birthsex{k},years_for_show(i));
        fprintf(1,'%s\n',addcommas(babynamedata(indices(i),k).totalcounts));
        fprintf(1,'Coverage: %4.2f\n',100*coverages(indices(i),k));
        fprintf(1,'\n');        

        if (i==1)
            fprintf(1,'RTD = %4.4f\n',RTDvals(indices(i),k));
            fprintf(1,'\n');
        end
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% find exclusive names in 2018 relative to 1968
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

index1 = 89;
index2 = 139;
year1 = years(index1);
year2 = years(index2);

for k=1:2
    j=1;
    elements(j).types = babynamedata(index1,k).names;
    elements(j).counts = babynamedata(index1,k).counts;

    [~,indices] = sort(elements(j).counts,'descend');
    elements(j).types = elements(j).types(indices);
    elements(j).counts = elements(j).counts(indices);
    elements(j).ranks = tiedrank(-elements(j).counts);
    elements(j).totalunique = length(elements(j).types);

    j=2;
    elements(j).types = babynamedata(index2,k).names;
    elements(j).counts = babynamedata(index2,k).counts;

    [~,indices] = sort(elements(j).counts,'descend');
    elements(j).types = elements(j).types(indices);
    elements(j).counts = elements(j).counts(indices);
    elements(j).ranks = tiedrank(-elements(j).counts);
    elements(j).totalunique = length(elements(j).types);

    mixedelements = combine_distributions(elements(1),elements(2));
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% truncation part
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% truncated version - bare
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %% system 1 and system 2, truncated

    settings1_2 = settings;
    settings1_2.plotkind = 'rank';
    %% settings.plotkind = 'probability';
    %% settings.plotkind = 'count';

    %% settings1_2.instrument = 'none';
    %% settings.instrument = 'rank divergence';
    %% settings.instrument = 'probability divergence';
    %% settings.instrument = 'alpha divergence type 2';

    settings1t_2t = settings1_2;

    settings1t_2t.turbulencegraph.labels = 'on';

    settings1t_2t.binwidth = 0.21;

    settings1t_2t.imageformat.open = 'no';

    settings1t_2t.cell_length = 1/15;
    settings1t_2t.maxrank_log10 = 5
    settings1t_2t.maxcount_log10 = 4;

    Orders = [1.5:.5:4.5];
    for i=1:length(Orders)
        Nval = floor(10^Orders(i));
        
        Nval1 = min(Nval,length(elements(1).types));
        Nval2 = min(Nval,length(elements(2).types));
        
        settings1t_2t.instrument = 'none';
        settings1t_2t.turbulencegraph.labels = 'on';

        if (k==1)
            settings1t_2t.system1_name = sprintf('Baby girl names in %d, top $10^{%.1f}$',...
                                                 year1,...
                                                 Orders(i));
            settings1t_2t.system2_name = sprintf('Baby girl names on %d, top $10^{%.1f}$',...
                                                 year2,...
                                                 Orders(i));
        else
            settings1t_2t.system1_name = sprintf('Baby boy names in %d, top $10^{%.1f}$',...
                                                 year1,...
                                                 Orders(i));
            settings1t_2t.system2_name = sprintf('Baby boy names on %d, top $10^{%.1f}$',...
                                                 year2,...
                                                 Orders(i));
        end
        
        tag = sprintf('babynames-rtd-010-bare-truncate-%d-%07d-%d-vs-%d',k,Nval,year1,year2);

        elements1t.types = elements(1).types(1:Nval1);
        elements1t.counts = elements(1).counts(1:Nval1);
        
        elements2t.types = elements(2).types(1:Nval2);
        elements2t.counts = elements(2).counts(1:Nval2);
        
        mixedelements1t_2t = ...
            combine_distributions(elements1t,elements2t);
        
        figallotaxonometer9000(mixedelements1t_2t,tag,settings1t_2t);
        
        %%% rank divergence
        settings1t_2t.instrument = 'rank divergence';

        settings1t_2t.alpha = Inf;
        settings1t_2t.turbulencegraph.labels = 'on';

        tag = sprintf('babynames-rtd-010-alpha-infty-rtd-truncate-%d-%07d-%d-vs-%d',k,Nval,year1,year2);

        figallotaxonometer9000(mixedelements1t_2t,tag,settings1t_2t);

        settings1t_2t.alpha = 0;
        settings1t_2t.turbulencegraph.labels = 'on';

        tag = sprintf('babynames-rtd-010-alpha-zero-rtd-truncate-%d-%07d-%d-vs-%d',k,Nval,year1,year2);

        figallotaxonometer9000(mixedelements1t_2t,tag,settings1t_2t);

        %%% thumbnail
        settings1t_2t.instrument = 'none';

        settings1t_2t.turbulencegraph.labels = 'off';

        tag = sprintf('babynames-rtd-010-thumbnail-truncate-%d-%07d-%d-vs-%d',k,Nval,year1,year2);

        figallotaxonometer9000(mixedelements1t_2t,tag,settings1t_2t);
        
        
    end

    if (k==1)
        %%        !combinepdfs figallotaxonometer9000/fig*babynames-rtd-010-bare-truncate-1*noname.pdf figallotaxonometer9000/figallotaxonometer9000-babynames-bare-truncated-1-combined.pdf
        %% !open figallotaxonometer9000/figallotaxonometer9000-babynames-bare-truncated-1-combined.pdf

        %%        !combinepdfs figallotaxonometer9000/fig*babynames-rtd-010-alpha-infty-rtd-truncate-1*noname.pdf figallotaxonometer9000/figallotaxonometer9000-babynames-rtd-alpha-infty-truncated-1-combined.pdf
        %% !open figallotaxonometer9000/figallotaxonometer9000-babynames-rtd-alpha-infty-truncated-1-combined.pdf

        %%        !combinepdfs figallotaxonometer9000/fig*babynames-rtd-010-alpha-zero-rtd-truncate-1*noname.pdf figallotaxonometer9000/figallotaxonometer9000-babynames-rtd-alpha-zero-truncated-1-combined.pdf
        %% !open figallotaxonometer9000/figallotaxonometer9000-babynames-rtd-alpha-zero-truncated-1-combined.pdf
    end
    if (k==2)
        %%        !combinepdfs figallotaxonometer9000/fig*babynames-rtd-010-bare-truncate-2*noname.pdf figallotaxonometer9000/figallotaxonometer9000-babynames-bare-truncated-2-combined.pdf
        %% !open figallotaxonometer9000/figallotaxonometer9000-babynames-bare-truncated-2-combined.pdf

        %%        !combinepdfs figallotaxonometer9000/fig*babynames-rtd-010-alpha-infty-rtd-truncate-2*noname.pdf figallotaxonometer9000/figallotaxonometer9000-babynames-rtd-alpha-infty-truncated-2-combined.pdf
        %% !open figallotaxonometer9000/figallotaxonometer9000-babynames-rtd-alpha-infty-truncated-2-combined.pdf

        %%        !combinepdfs figallotaxonometer9000/fig*babynames-rtd-010-alpha-zero-rtd-truncate-2*noname.pdf figallotaxonometer9000/figallotaxonometer9000-babynames-rtd-alpha-zero-truncated-2-combined.pdf
        %% !open figallotaxonometer9000/figallotaxonometer9000-babynames-rtd-alpha-zero-truncated-2-combined.pdf
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% pdf random comparison
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    j=3;
    elements(j).types = babynamedata(index1,k).names(randperm(length(babynamedata(index1,k).names)));
    elements(j).counts = babynamedata(index1,k).counts;
    elements(j).ranks = babynamedata(index1,k).ranks;
    elements(j).totalunique = length(elements(j).types);

    j=4;
    elements(j).types = babynamedata(index2,k).names(randperm(length(babynamedata(index2,k).names)));
    elements(j).counts = babynamedata(index2,k).counts;
    elements(j).ranks = babynamedata(index2,k).ranks;
    elements(j).totalunique = length(elements(j).types);

    mixedelements_rand = combine_distributions(elements(3),elements(4));

    settings.alpha = Inf;
    [divergence_elements,normalization] = ...
        rank_turbulence_divergence(mixedelements_rand,settings.alpha);
    RTDvals_rand(k) = sum(divergence_elements);
    
    %% system 1 exclusive (only appearing in system 1)
    indices = find(mixedelements(2).counts == 0);
    
    fprintf(1,'Number of types exclusive to %d for baby %s names:\n',year1,birthsex{k});
    fprintf(1,'%s of %s (%.1f\\%%)\n',...
            addcommas(length(indices)), ...
            addcommas(length(elements(1).types)), ...
            100*length(indices)/length(elements(1).types));

    %% switch between these for 2018, 1968 and both
    indices = find(mixedelements(1).counts == 0);
    %%    indices = find(mixedelements(2).counts == 0);
    %%    indices = find((mixedelements(1).counts > 0) & ...
    %%                   (mixedelements(2).counts > 0));
    
    fprintf(1,'Number of types exclusive to %d for baby %s names:\n',year2,birthsex{k});
    fprintf(1,'%s of %s (%.1f\\%%)\n',...
            addcommas(length(indices)), ...
            addcommas(length(elements(2).types)), ...
            100*length(indices)/length(elements(2).types));
    
    fprintf(1,'RTD for random version: %.3f\n',RTDvals_rand(k));

    settings.imageformat.open = 'no';
    
    if (k==1)
        settings.system1_name = sprintf('Baby girl names in %d',year1);
        settings.system2_name = sprintf('Baby girl names in %d',year2);
    else
        settings.system1_name = sprintf('Baby boy names in %d',year1);
        settings.system2_name = sprintf('Baby boy names in %d',year2);
    end
        
    tag = sprintf('babynames-rtd-010-random-%d-%03d-%d-vs-%d',k,yeardelta,year1,year2);
    figallotaxonometer9000(mixedelements_rand,tag,settings);
    
    if (k==1) %% -lyn
              %%        indices = 1:length(mixedelements(2).types);
        girlnames = mixedelements(2).types(indices);
        girlranks = mixedelements(2).ranks(indices);
        girlcounts = mixedelements(2).counts(indices);
        
        %%        indices_madison = find(~cellfun(@isempty,regexp(girlnames,'d+[aeoiuy]s+[aeoiuy]n+$')));
        indices_madison = find(~cellfun(@isempty,regexp(girlnames,'[aeiouy]l+[yi]+n+$')));
        [girlranks_sorted,indices_sorted] = sort(girlranks(indices_madison),'ascend');
        girlnames_sorted = girlnames(indices_madison(indices_sorted));
        girlcounts_sorted = girlcounts(indices_madison(indices_sorted));
    
        for i=1:length(girlnames_sorted)
            if (rem(girlranks_sorted(i),1) > 0.25)
            fprintf('`%s''\t(%s),\n',girlnames_sorted{i}, ...
                    addcommas(girlranks_sorted(i)));
            else
            fprintf('`%s''\t(%s),\n',girlnames_sorted{i}, ...
                    addcommas(girlranks_sorted(i)));
            end
        end
     end

    if (k==1) %% a-d-lyn
              %%        indices = 1:length(mixedelements(2).types);
        girlnames = mixedelements(2).types(indices);
        girlranks = mixedelements(2).ranks(indices);
        girlcounts = mixedelements(2).counts(indices);
        
        %%        indices_madison = find(~cellfun(@isempty,regexp(girlnames,'d+[aeoiuy]s+[aeoiuy]n+$')));
        indices_madison = find(~cellfun(@isempty,regexp(girlnames,'^A[aeiouy]*d+[aeiouy]l+[yi]+n+$')));
        [girlranks_sorted,indices_sorted] = sort(girlranks(indices_madison),'ascend');
        girlnames_sorted = girlnames(indices_madison(indices_sorted));
        girlcounts_sorted = girlcounts(indices_madison(indices_sorted));
    
        [~,indices_alphasort] = sort(girlnames_sorted);
        for i=1:length(girlnames_sorted)
            fprintf('`%s'' & \t(%s),\n',girlnames_sorted{indices_alphasort(i)}, ...
                    addcommas(girlranks_sorted(indices_alphasort(i))));
        end
    end

    
    if (k==2) %% aiden+
        boynames = mixedelements(2).types(indices);
        boyranks = mixedelements(2).ranks(indices);
        boycounts = mixedelements(2).counts(indices);
        
        indices_aiden = find(~cellfun(@isempty,regexp(boynames,'[Aa][iy]*d+[aeoiuy]n+$')));
        [boyranks_sorted,indices_sorted] = sort(boyranks(indices_aiden),'ascend');
        boynames_sorted = boynames(indices_aiden(indices_sorted));
        boycounts_sorted = boycounts(indices_aiden(indices_sorted));
    
        for i=1:length(boynames_sorted)
            if (rem(boyranks_sorted(i),1) > 0.25)
            fprintf('`%s''\t(%s),\n',boynames_sorted{i}, ...
                    addcommas(boyranks_sorted(i)));
            else
            fprintf('`%s''\t(%s),\n',boynames_sorted{i}, ...
                    addcommas(boyranks_sorted(i)));
            end
        end
    end
end





%% save useful things
save figbaby_names_divergence050 years RTDvals RTDvals_rand

more on;

