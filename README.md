Suggested set up on Mac/Unix/Linux machines:

1. Script directory location:
~/matlab/allotaxonometry  (This location is really required or startup.m won't work without modification.)

2. Add following snippet to ~/matlab/startup.m

Explanation:
- ~/matlab is placed Matlab's path by default
- genpath will find subdirectories in ~/matlab to add to path
- remove ones we don't need with regexprep (.git and archive; adjust as needed)
- add to paths with addpath(paths_to_add)

```
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

%% add all subdirectories in ~/matlab
paths_to_add = genpath('~/matlab');
%% remove .git, archive subdirectories
paths_to_add = regexprep(paths_to_add,'~\/matlab[^:]*?\/\.git.*?:','','all');
paths_to_add = regexprep(paths_to_add,'~\/matlab\/archive.*?:','','all');

%% add paths
addpath(paths_to_add);

%% current directory
addpath .

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
```

3. the command print_universal constructs pdfs and opens them
   - ideally, matlab figure viewer should never be used
   - pdf is a final product
   - option for png is available; requires imagemagic (see script for instructions)
   - print_universal logs figure making activity (see script for details)
