function print_universal(filename,imageformat)
%% print_universal(filename,imageformat
%% 
%% prints current figure according to prescription in imageformat structure
%% 
%% example:
%% imageformat.type = 'pdf';
%% imageformat.dpi = 600;
%% imageformat.deleteps = 'yes';
%% imageformat.open = 'no';
%% imageformat.copylink = 'no';
%% 
%%
%%%%%%%%%% 
%% 
%% requires ImageMagick for png production
%% 
%% https://imagemagick.org/index.php
%% 
%% recommended installation method: homebrew (https://brew.sh):
%% https://formulae.brew.sh/formula/imagemagick@6#default
%% 
%%%%%%%%%%
%%
%% for analysis of figure making/self productivity, script logs
%% - the timestamp, 
%% - the  size in bytes, 
%% - and full path filename 
%% for every figure, storing output in text file:
%% ~/work/log/figures/YYYY-MM
%% 
%% format example:
%% 2019 08 05 14 40 33.6106 164776 /Users/dodds/work/stories/2018-06story-turbulence/figures/figstoryturbulence_timeseries001/figstoryturbulence_timeseries001_election004_noname.pdf
%% 
%% example quick inspection of figure making activity: 
%% wc ~/work/log/figures/*
%%
%% 
%% peter sheridan dodds
%% https://gitlab.com/petersheridandodds
%% https://github.com/petersheridandodds
%% MIT License

cheese_error001;

if (strcmp(imageformat.type,'pdf'))
    dpistr = sprintf('-r%d',imageformat.dpi);

    filenameps = sprintf('%s.ps',filename);
    filenamepdf = sprintf('%s.pdf',filename);
    fprintf(1,'printing (colour) to:\n%s.ps\n',filename);
    print(filenameps,'-depsc2',dpistr);
    
    fprintf(1,'converting to:\n%s.pdf\n',filename);
    pdfcommand = sprintf('epstopdf %s',filenameps);
    system(pdfcommand);
    
    if (strcmp(imageformat.deleteps,'yes'))
        disp('deleting postscript...');
        rmcommand = sprintf('\\rm %s',filenameps);
        system(rmcommand);
    end

    %% write to log
    homedir = getenv('HOME');
    cdir = pwd;
    timestamp = datevec(now);

    dirname = sprintf('%s/work/log/figures',homedir);
    if (exist(dirname,'dir'))
        logfile = sprintf('%s/work/log/figures/%d-%02d',homedir,timestamp(1),timestamp(2));
        fid = fopen(logfile,'a');

        command = sprintf('ls -l %s| awk ''{print $5}''',filenamepdf);
        [status,bytes] = system(command);
        %% remove carriage return
        bytes = bytes(1:end-1);

        fprintf(fid,'%d %02d %02d %02d %02d %g %s %s/%s\n',timestamp(1),timestamp(2),timestamp(3),timestamp(4),timestamp(5),timestamp(6),bytes,cdir,filenamepdf);

        fclose(fid);

        fprintf(1,'figure data logged.\n');
    end
elseif (strcmp(imageformat.type,'png')) 
    dpistr = sprintf('-r%d',imageformat.dpi);
    
    
    disp(sprintf('printing to\n%s.png\n',filename));
    filenamepng = sprintf('%s.png',filename);

    print(filenamepng,'-dpng',dpistr);

    bordersize = imageformat.bordersize;
    tmpcommand = ...
        sprintf('convert -trim +repage -bordercolor white -border %d %s %s',bordersize,filenamepng,filenamepng);
    system(tmpcommand);

    %% write to log
    homedir = getenv('HOME');
    cdir = pwd;
    timestamp = datevec(now);

    dirname = sprintf('%s/work/log/figures',homedir);
    if (exist(dirname,'dir'))
        logfile = sprintf('%s/work/log/figures/%d-%02d',homedir,timestamp(1),timestamp(2));
        fid = fopen(logfile,'a');

        command = sprintf('ls -l %s| awk ''{print $5}''',filenamepng);
        [status,bytes] = system(command);
        %% remove carriage return
        bytes = bytes(1:end-1);

        fprintf(fid,'%d %02d %02d %02d %02d %g %s %s/%s\n',timestamp(1),timestamp(2),timestamp(3),timestamp(4),timestamp(5),timestamp(6),bytes,cdir,filenamepng);

        fclose(fid);

        fprintf(1,'figure data logged.\n');
    end
end

if (strcmp(imageformat.open,'yes'))
    tmpcommand = sprintf('open %s.%s;',filename,imageformat.type);
    system(tmpcommand);
end

if (strcmp(imageformat.copylink,'yes'))
    tmpcommand = sprintf('printf ''%s.%s'' | pbcopy',filename,imageformat.type);
    system(tmpcommand);
end

