function elements_smooshed = smoosh_distributions(elements)
%% elements_smooshed = smoosh_distributions_add(elements)
%% 
%% combination of distributions by adding counts, and, if present,
%% averaging of probabilities.
%% 
%% expects input of the form of a vector structures of matching form.
%% 
%% two pieces are required for each system:
%% 
%% elements(i).types (cell array, names of things)
%% elements(i).counts (counts of things, more generally may be sizes
%% of things)
%% 
%% optional:
%% elements(i).probs
%% 
%% computed:
%% 
%% %% 
%% 
%% elements_added.ranks (ranks of things based on counts or sizes)
%% if present in elements(i)
%% elements1.probs ( = elements1.counts/elements1.totalcount)
%% elements_added.totalcount (overall count for normalization)

%% produces:
%% 
%% elements_smooshed of same form as elements

%% create overall list of types:
elements_smooshed.types = elements(1).types;
for i=2:length(elements)
    elements_smooshed.types = ...
        union(elements_smooshed.types,...
              elements(i).types,...
              'stable');
end

%% number of unique types
N = length(elements_smooshed.types);

%% initialize counts and, if appropriate, probs
elements_smooshed.counts = zeros(N,1);
if (isfield(elements(1),'probs'))
    elements_smooshed.probs = zeros(N,1);
end

for i=1:length(elements)
    [presence,indices] = ...
        ismember(elements_smooshed.types,...
                 elements(i).types);
    newindices = find(presence==1);

    elements_smooshed.counts(newindices) = ...
        elements_smooshed.counts(newindices) + ...
        elements(i).counts(indices(newindices));

    if (isfield(elements(i),'probs'))
        elements_smooshed.probs(newindices) = ...
            elements_smooshed.probs(newindices) + ...
            elements(i).probs(indices(newindices)) / ...
                              length(elements);
    end
end

elements_smooshed.totalcount = sum(elements_smooshed.counts);


%% compute ranks according to counts
elements_smooshed.ranks = tiedrank(-elements_smooshed.counts);

%% compute ranks according to probs
elements_smooshed.ranks_probs = tiedrank(-elements_smooshed.counts);

