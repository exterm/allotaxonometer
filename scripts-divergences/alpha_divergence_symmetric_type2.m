function [divergence,divergence_elements] = alpha_divergence_symmetric_type2(p1,p2,alpha)
%% [divergence,divergence_elements] = alpha_divergence_symmetric_type2(p1,p2,alpha)
%% 
%% Or: Generalized Jenson-Shannon divergence
%% Or: -> Symmetric Generalized Entropy divergence 
%% 
%% returns alpha divergence, symmetric, type 2
%% and divergence component for each type
%%
%% logs are natural logs, entropy is measured in nats
%% 
%% p1 and p2 must be of the same length and correspond to the same
%% elements
%% 
%% for pairs of systems which have elements appearing in only one
%% system, divergence will be finite for -\infty < \alpha < 1
%% 
%% Eq. 37 (see unnumbered version below Eq. 40) in:
%% "Families of Alpha- Beta- and Gamma- Divergences: Flexible and
%% Robust Measures of Similarities"
%% Cichocki and Amari
%% Entropy, Vol. 12, pp. 1543--1568, 2010.
%% 

pavg = 0.5*(p1 + p2);

if (alpha == 0) %% Jensen-Shannon divergence
    divergence_elements = 0.5*p1.*log((p1 + (p1==0))./pavg) + ...
        0.5*p2.*log((p2 + (p2==0))./pavg);
elseif (alpha == 1) %% arithmetic-geometric divergence
                    %% will be infinite if types are not all present
                    %% in both systems (i.e., there are eany zero
                    %% probabilities)
    divergence_elements = + 0.5*(p1 + p2).*(...
        log((p1+p2)/2) ...
        - 0.5*log(p1) ...
        - 0.5*log(p2) ...
        );
else
    divergence_elements = 1/(2*alpha*(alpha-1)) * ...
        ((p1.^(1-alpha) + p2.^(1-alpha)).*pavg.^alpha - 2*pavg);
end
    
divergence_elements(find(divergence_elements<0)) = 0;

divergence = sum(divergence_elements);
