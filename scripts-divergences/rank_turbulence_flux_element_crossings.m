function [indices_up,indices_down] = rank_turbulence_flux_element_crossings(mixedelements,rank)
%% 
%% [indices_up,indices_down] = rank_turbulence_flux_element_crossings(mixedelements,rank)
%% 
%% returns indices of elements moving up to and above rank _rank_
%% and indices of elements moving below rank _rank_

%% 1/2. upwards flux:

indices_up = find( ...
    (mixedelements(1).ranks > rank) ...
    & ...
    (mixedelements(2).ranks <= rank));

%% now sort elements moving up by ranks2

[tmp,indices] = sort(mixedelements(2).ranks(indices_up),'ascend');
indices_up = indices_up(indices);

%% 2/2. downwards flux:

indices_down = find( ...
    (mixedelements(1).ranks <= rank) ...
    & ...
    (mixedelements(2).ranks > rank));

%% now sort elements moving down by ranks2

[tmp,indices] = sort(mixedelements(2).ranks(indices_down),'ascend');
indices_down = indices_down(indices);
