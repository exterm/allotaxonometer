function [divergence_elements,normalization,contribution_indices] = probability_turbulence_divergence(mixedelements,alpha)
%% [divergence_elements,normalization,contribution_indices] = probability_turbulence_divergence(mixedelements,alpha)
%% 
%% returns per type probility turbulence adivergence values for alpha
%% 
%% alpha >= 0 and may be specified as Inf
%% 
%% - divergence_elements: per eleemnt contributions (sum = PTD)
%% - normalization, separate diagnostic scalar (adjusted for alpha=0
%%   (see paper)
%% - contribution_indices: order of elements in decreasing
%%   contribution to overall divergence;
%%   straightforward except for alpha=0 (se paper)
%% 
%% for mixedelements construction, use combine_distributions

x1 = mixedelements(1).probs;
x2 = mixedelements(2).probs;

if (sum(find((x1==0) & (x2==0))) > 0)
    error(['Mixed system contains types which do not appear in either ' ...
           'system; please check']);
end

if (~(abs(sum(x1) - 1) < 10^-10))
    fprintf(1,['Normalizing first system''s probabilities as sum was ' ...
               'not within 10^-10 of 1; please check\n']);
    x1 = x1/sum(x1);
end
if (~(abs(sum(x2) - 1) < 10^-10))
    fprintf(1,['Normalizing second system''s probabilities as sum was ' ...
               'not within 10^-10 of 1; please check\n']);
    x2 = x2/sum(x2);
end

if (alpha < 0)
    error('alpha must be >= 0');
elseif (alpha == Inf)
    divergence_elements = max(x1,x2);
    divergence_elements(find(x1==x2)) = 0;
    [~,contribution_indices] = sort(divergence_elements,'descend');
elseif (alpha == 0)
    %% 1s or 0s, 1/alpha limit removed
    divergence_elements = ...
        (x1==0) + (x2==0);

    %% ordering

    %% types appear in both systems
    %% (secondary to exclusive types in ordering)
    indices_both = find((x1>0) & (x2>0));
    weights(indices_both) = abs(log(x1(indices_both)./x2(indices_both)));
    maxweight = max(weights);

    %% types only appear in system 1 or only in system 2
    %% (corresponds to divergence_elements)
    %% (primary ordering)
    indices_exclusive = find((x1==0) | (x2==0));
    weights(indices_exclusive) = maxweight + x1(indices_exclusive) + x2(indices_exclusive);
    
    [~,contribution_indices] = sort(weights,'descend');
else
    divergence_elements = ...
        (alpha+1)/alpha* ...
        (abs(x1.^alpha - x2.^alpha)).^(1./(alpha+1));
    [~,contribution_indices] = sort(divergence_elements,'descend');
end

%% normalization
%% treat as disjoint
N1 = sum(x1>0);
N2 = sum(x2>0);

if (alpha == Inf)
    normalization = 2;
elseif (alpha == 0)
    normalization = N1 + N2;
else
    normalization = ...
        (alpha+1)/alpha * ...
        sum(x1.^(alpha./(alpha+1))) ...
        + ...
        (alpha+1)/alpha * ...
        sum(x2.^(alpha./(alpha+1)));
end

divergence_elements = ...
    divergence_elements / normalization;
